[![pipeline status](https://gitlab.com/opengeoweb/geoweb-theme/badges/master/pipeline.svg)](https://gitlab.com/opengeoweb/geoweb-theme/-/commits/master)
[![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/geoweb-theme/raw/master/package.json)](https://gitlab.com/opengeoweb/geoweb-theme/-/tags)

# KNMI GeoWeb Theme

### Installation

To run the KNMI Theme storybook you need npm. The easiest way to install npm is via nvm. Please visit https://nvm.sh/ and follow the instructions. When nvm is installed, please run the following command:

```
nvm install 12
```

### To run the storybook

```
npm ci
npm run storybook
```

### Storybook

#### Inspecting the components with storybook

Storybook can be used to view the components. If you run `npm run storybook` a storybook server will start in which you can see all the components for which stories are written.

#### Writing a new story

Stories live in the stories folder. Here is the documentation on the syntax for adding new stories:
https://storybook.js.org/docs/basics/writing-stories/

#### Deploying a static version of your storybook

The storybook can be compiled to a static version via npm run build-storybook. The static contents are then placed in the folder storybook-geoweb-core.

### Managing packages

Follow the instructions below for dependency management. Please refer to [the documentation](https://docs.npmjs.com/cli-documentation/) for more information on any of the specific commands.

#### Removing extraneous packages

During development, we may end up with packages that have been installed but are not actually being used. In order to remove them, run `npm prune`.

#### Adding a new dependency

Adding a new dependency can be done following these steps:

1. `npm install <package-name>` or `npm install <package-name> --save-dev`
2. commit the updated files package.json and package-lock.json

Make sure to add the package as a dev depencency when it's only used for development purposes (like storybook or linting).

#### Updating an existing dependency

To see a list of which packages could need updating, run `npm outdated -l`.
A red package name means there’s a newer version matching the semver requirements in the package.json, so it can be updated. Yellow indicates that there’s a newer version higher than the semver requirements in the package.json (usually a new major version), so proceed with caution.

To update a single package to its latest version that matches the version required in package.json (red in the list), run `npm update <package-name>`, and commit the updated package-lock.json file.

To update all packages at once (that are red in the list), run `npm update`, and commit the updated package-lock.json file.

To update a single package to a version higher than the required version in package.json (yellow in the list):

1. check if there are any breaking changes to be aware of in the new version
2. `npm install <package-name>@<version>` or `npm install <package-name>@<version> --save-dev`
3. commit the updated files package.json and package-lock.json
4. update the code related to breaking changes

After any version change, make sure to test if everything still works.

#### Checking for vulnerabilities

To scan the project for any known vulnerabilities in existing dependencies, you can run `npm audit`. To fix them:

1. `npm audit fix`
2. commit the updated package-lock.json
3. test if everything still works

### Tests

Tests can be run by the following command: `npm test`. This will run all the tests specified in the stories without starting a browser.
In order to see a coverage report, run: `npm run test:coverage`

#### Testing the package in another project

To test the package in another project without publishing it, you can follow these steps:
In geoweb-theme:

1. `npm run package`

In yourproject:

1. `cd <path/to/yourproject>`
2. `npm install <absolute path/to/tar/created/by/npm pack>`

#### Bundling, packaging and publishing to NPM

We use Semantic Versioning, see the [documentation](https://semver.org) for details.

1. Decide how to bump the version, is it a major, minor or patch version?
2. Create a new branch based off master and ensure there is an upstream branch BEFORE going to the next step
3. Update the version by running `npm version major|minor|patch` on the new branch
4. Merge the branch into master
5. Go to the gitlab pipeline of master and run the publish stage
6. Check for the new version on [npm](https://www.npmjs.com/package/@opengeoweb/theme) and [gitlab](https://gitlab.com/opengeoweb/geoweb-theme/-/tags)

### License

This project is licensed under the terms of the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.

Every file in this project has an header that specifies the licence and copyright. It is possible to add/remove the licence header using the following commands:

- `npm run licence:add` => it adds to all possible files the licence header if the file has not an header with the same content of the LICENCE file. This action can require a manual check in the case in which the file contains a wrong header (i.g. a simple word is missed), because it just adds another header without removing the wrong one.

- `npm run licence:remove` => it removes the specific header from all files that contain an header with the same content of the LICENCE file. This action can require a manual check in the case in which the file contains a wrong header (i.g. a simple word is missed), because it just removes the correct header and not the wrong one.
