/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import * as React from 'react';
import {
  Button,
  ButtonGroup,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  InputLabel,
  makeStyles,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  Tab,
  Tabs,
  TextField,
  ThemeProvider,
  Tooltip,
  Typography,
} from '@material-ui/core';
import NotificationsIcon from '@material-ui/icons/Notifications';

import { GWTheme } from '.';

export default { title: 'theme' };

const useStyles = makeStyles({
  sectionTitle: {
    fontSize: 18,
    borderBottom: '1px solid black',
  },
});

// TODO: this is just a part of all styles, there are more defined in theme.tsx. It would be nice to have a complete overview so we can use it as a styleguide.
const DemoComponent: React.FC<{}> = () => {
  const classes = useStyles();

  return (
    <Grid container spacing={1}>
      {/** Typography */}
      <Grid item xs={12}>
        <Typography variant="h1" className={classes.sectionTitle}>
          Typography
        </Typography>
        <Typography variant="h6">h6</Typography>
        <Typography variant="subtitle1">subtitle1</Typography>
        <Typography variant="subtitle2">subtitle2</Typography>
        <Typography variant="body1">body1</Typography>
        <Typography variant="body2">body2</Typography>
        <Typography variant="overline">overline</Typography>
        <Typography variant="caption">caption</Typography>
        <Typography variant="button">button</Typography>
      </Grid>
      {/** MUI Elements */}
      <Grid item xs={12} container spacing={1}>
        <Grid item xs={12}>
          <Typography variant="h1" className={classes.sectionTitle}>
            Form elements
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <ButtonGroup>
            <Tooltip title="Button tooltip">
              <Button color="secondary">One</Button>
            </Tooltip>
            <Button color="secondary">Two</Button>
            <Button color="secondary">Three</Button>
          </ButtonGroup>
        </Grid>
        <Grid item xs={12}>
          <Tabs
            indicatorColor="secondary"
            textColor="secondary"
            variant="scrollable"
            scrollButtons="auto"
            value="ONE"
          >
            <Tab
              label="One"
              value="ONE"
              icon={<NotificationsIcon fontSize="small" color="action" />}
            />

            <Tab label="Two" value="TWO" />
          </Tabs>
        </Grid>
        <Grid item xs={12}>
          <TextField
            label="TextfieldLabel"
            value="Content"
            helperText="Some helpertext"
          />
          &nbsp;
          <TextField
            label="TextfieldLabel"
            value="Disabled textfield"
            helperText="Some helpertext"
            disabled
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            label="Textfield"
            value="Text content filled variant"
            helperText="Some helpertext"
            variant="filled"
          />
          <TextField
            label="Textfield"
            value="But this one is disabled"
            helperText="Some helpertext"
            disabled
            variant="filled"
          />
        </Grid>
        <Grid item xs={12}>
          <FormControl component="fieldset">
            <FormLabel component="legend">Gender</FormLabel>
            <RadioGroup
              name="Radio Button"
              value="Clouds"
              onChange={
                // eslint-disable-next-line @typescript-eslint/no-empty-function
                (): void => {}
              }
            >
              <FormControlLabel
                value="Clouds"
                control={<Radio />}
                label="Clouds"
              />
              <FormControlLabel value="Rain" control={<Radio />} label="Rain" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl>
            <InputLabel>Select</InputLabel>
            <Select
              value={20}
              onChange={
                // eslint-disable-next-line @typescript-eslint/no-empty-function
                (): void => {}
              }
              autoWidth={false}
              style={{ width: '200px' }}
            >
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>
                Thirty and some really long text stuff
              </MenuItem>
            </Select>
          </FormControl>
          &nbsp;
          <FormControl>
            <InputLabel>Select Disabled</InputLabel>
            <Select
              value={20}
              onChange={
                // eslint-disable-next-line @typescript-eslint/no-empty-function
                (): void => {}
              }
              disabled
              autoWidth={false}
              style={{ width: '200px' }}
            >
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>
                Thirty and some really long text stuff
              </MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl>
            <InputLabel>Select Filled</InputLabel>
            <Select
              value={20}
              onChange={
                // eslint-disable-next-line @typescript-eslint/no-empty-function
                (): void => {}
              }
              autoWidth={false}
              style={{ width: '200px' }}
              variant="filled"
            >
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>
                Thirty and some really long text stuff
              </MenuItem>
            </Select>
          </FormControl>
          &nbsp;
          <FormControl>
            <InputLabel>Select Filled Disabled</InputLabel>
            <Select
              value={20}
              onChange={
                // eslint-disable-next-line @typescript-eslint/no-empty-function
                (): void => {}
              }
              disabled
              autoWidth={false}
              style={{ width: '200px' }}
              variant="filled"
            >
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>
                Thirty and some really long text stuff
              </MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            control={
              <Checkbox
                defaultChecked
                color="secondary"
                inputProps={{ 'aria-label': 'checkbox' }}
              />
            }
            label="Checkbox"
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

export const WithTheme: React.FC<{}> = () => (
  <ThemeProvider theme={GWTheme}>
    <DemoComponent />
  </ThemeProvider>
);

export const WithoutTheme: React.FC<{}> = () => <DemoComponent />;
