/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

import { createMuiTheme } from '@material-ui/core';
import { AlertClassKey } from '@material-ui/lab/Alert';

// TODO: [Loes 17-07-2020] Remove this when we upgrade to materialUI version 5.0.0, this should be included in that release
declare module '@material-ui/core/styles/overrides' {
  export interface ComponentNameToClassKey {
    MuiAlert: AlertClassKey;
  }
}

export const GWTheme = createMuiTheme({
  palette: {
    secondary: { main: '#0075a9' },
    action: { active: '#ffa800' },
    text: { disabled: '#051039' },
  },
  shape: { borderRadius: 3 },
  typography: {
    h6: {
      fontSize: '16px',
    },
    subtitle1: {
      fontSize: '16px',
      fontWeight: 500,
      lineHeight: 1.5,
      letterSpacing: '0.15px',
      opacity: 0.87,
      color: '#051039',
    },
    subtitle2: {
      fontSize: '14px',
      fontWeight: 500,
      lineHeight: 1.71,
      letterSpacing: '0.1px',
      opacity: 0.87,
      color: '#051039',
    },
    body1: {
      fontSize: '16px',
      lineHeight: 1.56,
      letterSpacing: '0.44px',
      color: '#051039',
    },
    body2: {
      fontSize: '14px',
      lineHeight: 1.43,
      letterSpacing: '0.25px',
      color: '#051039',
    },
    overline: {
      textTransform: 'none',
      fontSize: '12px',
      lineHeight: 1.43,
      letterSpacing: '0.25px',
      color: '#051039',
      opacity: 0.7,
    },
    caption: {
      fontSize: '12px',
      lineHeight: 1.33,
      letterSpacing: '0.4px',
      color: '#051039',
      opacity: 0.7,
    },
    button: {
      fontWeight: 500,
      fontSize: '14px',
      color: '0075a9',
    },
  },
  overrides: {
    MuiTooltip: {
      tooltip: {
        borderRadius: '2px',
        fontSize: '16px',
        fontWeight: 'normal',
        padding: '10px',
        backgroundColor: '#484848',
      },
    },
    MuiIconButton: {
      root: {
        color: '#0075a9',
      },
    },
    MuiCheckbox: {
      root: {
        color: '#0075a9',
      },
    },
    MuiSelect: {
      icon: {
        color: '#0075a9',
      },
      select: { '&:focus': { backgroundColor: 'rgba(0, 117, 169, 0.05)' } },
    },
    MuiAlert: {
      standardWarning: {
        border: '1px solid #ff9800',
        borderRadius: '0px',
      },
      action: { color: '#0075a9' },
      message: {
        fontSize: '14px',
        lineHeight: 1.43,
        letterSpacing: '0.25px',
        color: '#051039',
      },
    },
    MuiTabs: {
      root: {
        backgroundColor: '#ffffff',
        minHeight: '30px',
      },
      scrollButtons: {
        width: '20px',
      },
    },
    MuiTab: {
      root: {
        minHeight: '30px',
        textTransform: 'none',
        minWidth: '76px', // 100 minus 24 padding (12 left and 12 right)
        '@media (min-width: 600px)': {
          minWidth: '76px',
        },
      },
      labelIcon: {
        minHeight: '30px',
      },
    },
    MuiListItem: {
      root: {
        '&$selected': {
          backgroundColor: 'rgba(0, 117, 169, 0.15)',
          borderLeft: '4px solid #0075a9',
          '&:hover': {
            backgroundColor: 'rgba(0, 117, 169, 0.10)',
          },
        },
      },
      button: {
        '&:hover': {
          backgroundColor: 'rgba(0, 117, 169, 0.15)',
        },
      },
    },
    MuiFormLabel: {
      root: {
        color: 'rgba(5, 16, 57, 0.7)',
        '&$focused': {
          color: '#0075a9',
        },
      },
    },

    MuiOutlinedInput: {
      root: {
        '&$focused $notchedOutline': {
          borderColor: '#0075a9',
        },
      },
    },
    MuiFilledInput: {
      root: {
        backgroundColor: 'rgba(0, 117, 169, 0.05)',
        color: '#051039',
        '&$disabled': { backgroundColor: 'transparent' },
      },
      underline: { '&$disabled:before': { borderBottomStyle: 'none' } },
    },
    MuiCardContent: {
      root: {
        '&:last-child': {
          paddingBottom: 0,
        },
      },
    },
  },
});
